package SVG;

import java.util.Locale;

public class Text extends Tag2{
    private String s;
    private double x;
    private double y;
    private double fontSize;

    public Text(String s, double x, double y, int fontSize){
        super("text");
        this.s=s;
        this.x=x;
        this.y=y;
        this.fontSize= fontSize;
    }

    public String getParameters() {

        return String.format(Locale.US, "x=\"%f\" y=\"%f\" font-family=\"Verdana\" font-size=\"%f\"", x, y, fontSize);

    }

    @Override
    public String getContent() {
        return s;
    }
}
