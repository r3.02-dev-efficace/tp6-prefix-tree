package Tries;

public class Chain implements ItemWithKey {
        private String s;

        public Chain(String s){
            this.s=s;
        }

        public String getS() {
            return s;
        }

        @Override
        public String toString() {
            return s;
        }

        @Override
        public int getKeyLength() {
            return s.length();
        }

        @Override
        public int getBase() {
            return 256;
        }

        @Override
        public int getDigit(int j) {
            return (int)s.charAt(j);
        }

}
