package Tries;

import SVG.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


    /*
    Version où l'on stocke chaque élément tout au bout du chemin de sa clef.
    Les clefs peuvent ne pas être propres (on peut avoir clef préfixe d'une autre)

    todo : améliorer pour construire qu'une fois en static un tableau de PtreeF vides, mais je sais pas faire sans prendre d'elem
    en param pour avec taille (attribut private  final static ArrayList<PtreeF<T>> tabPtreeFVides; car static pas ok avec <T>

    */

public class Ptree<T extends ItemWithKey>{


    /************************************************************************
     attributs
     ************************************************************************/
    private ArrayList<Ptree<T>> t; //tableau des fils
    private T e;// élément

        /*
        Rmqs:
        -Lorsque les clefs ne sont pas propres on peut avoir e!=null,  et d'autres éléments en dessous.
        -Même convention que listes : vide <=>  t==null
        -Si !vide, alors t non null et contient dans toutes cases pointeurs vers PtreeF (jamais de case valant null)
        -Par définition des Ptree, si c'est une Feuille, alors e!= null  (feuille : PtreeF tq t ne que contient que des pointeurs vers arbres vides)



        Rmq sur les débats liés à la modélisation (pourquoi ne pas mettre des t[i] à null plutot que vide lorsque l'on a pas de fils num i,
        et pourquoi pour une feuille ne pas mettre carrément t à null plutôt qu'un tableau d'arbres vides) :
              Raison : par uniformité avec classes Arbre et Liste vu avant, et donc même arguments :
                - pour l'invariant "jamais de t non null contenant un /des pointeurs null" : parallèle avec classe Arbre, on a jamais un vrai filsG et un filsD null
                (et donc pour tout arbre non vide on peut faire des appels rec sur tous ses fils sans devoir vérifier si il y a des fils égaux à null)
                - penser aux listes : si on faisait l'optimisation ici de "une feuille = l'élément et t=null", alors en faisant de même pour une liste,
                 le dernier maillon serait un élément, et suiv=null, et donc penser au code du toString par ex qui devient laid car il faut tester
                 si vide, puis si suiv==null
                - pour l'inconvénient : ça gâche de la mémoire : on peut optimiser en déclarant un static Ptree[] tabVide contenant getMaxDigit PtreeF vides
                (c'est le rôle de l'attribut "tvide" ici)
        */

    //attributs seulement utiles pour le dessin en svg (à ignorer donc)
    private final static int fontsizeElem = 5;
    private final static double hboxTab =  fontsizeElem;
    private final static String textTnull = "t = null";
    private final static String textEnull = "e = null";
    private final static String textEmpty = "()";



    //Attributs pour l'optimisation des méthodes "listAll" (à ne pas inclure au départ, ou faire autre classe si on veut séparer).
    //Ces attributs pourraient être maintenus par les méthodes add/delete, mais pour simplifier on choisit de ne
    //les calculer que lorsque le Ptree est fini de construire (c'est à dire n'aura plus de add ou delete)
    private ArrayList<Integer> listeFilsNonVides = new ArrayList<>(); //pour lister les indices des fils non vides
    //private  static ArrayList<Ptree<T>> tvide; static et generique pas possible..
    private ArrayList<Ptree<T>> raccourcis = new ArrayList<>(); //pour que raccourcis.get(i) pointe vers le prochain
    //noeud de branchement ou contenant un élément du fils i (ou vide si le fils i est vide)


    //private ArrayList<Integer> prefD; pour le débug, sert à stocker le préfixe courant dans chaque sous arbre

        /*public void remplirPrefD(ArrayList<Integer> p){
            //pour le début : sert à accrocher à chaque noeud son préfixe correspondant
            prefD = new ArrayList<>(p);
            if(!isEmpty())
            {
                int i = 0;
                for(PtreeF<T> fils : t){
                    p.add(i);
                    fils.remplirPrefD(p);
                    p.remove(p.size()-1);
                    i++;
                }
            }
        }*/


    /************************************************************************
     méthodes basiques
     ************************************************************************/
    public Ptree(){}

    //get et set utiles pour les tests, pour pouvoir construire des Arbres bizarres et tester qu'il ne sont pas des Ptree
    public ArrayList<Ptree<T>> getT() {
        return t;
    }

    public void setT(ArrayList<Ptree<T>> t) {
        this.t = t;
    }

    public T getE() {
        return e;
    }

    public void setE(T e) {
        this.e = e;
    }


    /************************************************************************
     section principale (add, delete, ..)
     ************************************************************************/
    public int nbSommets(){

        //retourne nombre de sommets de l'arbre "maths", c'est à dire l'arbre correspondant à this dans lequel on ne compte pas tous les arbres vides
        //(retourne donc le nombre de Ptree non vides contenus dans this)
        //motivation : pour mesurer à quel point en pratique on est loin de l'inégalité nbSommets <= size()*L
        //avec L taille de la plus grande clef d'un élément

        if(isEmpty())return 0;
        int res = 1;
        for(Ptree<T> fils : t)
            res+=fils.nbSommets();
        return res;

    }

    public int nbSommetsTotal() {
        //retourne nombre de sommets de l'arbre en comptant les arbres vides


        if(isEmpty())return 1;
        int res = 1;
        for(Ptree<T> fils : t)
            res+=fils.nbSommetsTotal();
        return res;
    }

    public int getHeight(){
        //retourne hauteur (sans compter les arbres vides), donc on doit avoir en particulier hauteur = max(e.getKeyLength())+1

        if(isEmpty()) return 0;
        int res=0;
        for(Ptree<T> fils: t){
            res=Math.max(res,fils.getHeight());
        }
        return res+1;
    }

    public int size(){
        //retourne le nombre d'éléments
        if(isEmpty()) return 0;
        int res=0;
        if(e!=null)
            res++;
        for (Ptree<T> fils : t) {
            res +=  fils.size();
        }
        return res;
    }

    public boolean isEmpty(){
        return t==null; // ajouter "&& e==null" est inutile par convention
    }

    public boolean add(T e){
        //action : essaye d'ajouter e à this, et retourne true ssi l'ajout a eu lieu
        return add(e,0);
    }

    public boolean add(T el, int i){
        //prérequis : 0 <= i <= el.getKeyLength()
        //action : soit k = el.getKey()[i..] (c'est à dire la sous clef de el entre l'indice i est la fin)
        //alors, si existe e' tq e'.getKey()[i..]=k, ne fait rien et retourne false,
        // sinon ajoute el en considérant que sa clef est k, et retourne true

        //optimisation mémoire, lors du premier add on prépare le tableau qui servira à toutes les feuilles
        //if(tvide==null){tvide = buildTabPtreeVides(el.getMaxDigit()+1);}

        if(i==el.getKeyLength()){
            if(e!=null)
                return false;
            else{
                e=el;
                if(isEmpty()) {
                    t = buildTabPtreeVides(el.getBase());//tvide;
                }
                return true;
            }
        }
        else{
            int d = el.getDigit(i);
            if(!isEmpty()){
                return t.get(d).add(el,i+1);
            }
            else{
                t = buildTabPtreeVides(el.getBase());//tvide;
                return t.get(d).add(el, i + 1);//ça sera return true
            }
        }

    }
    public boolean isPtree(){
        try {
            return isPtree(new ArrayList<Integer>());
        }
        catch(NullPointerException e){
            System.out.println("Null Pointer Exception dans isPtree()");
            return false;
        }
    }
    public boolean isPtree(ArrayList<Integer> p) {
        //retourne vrai ssi this est tel que
        // - invariants de modélisations vrais :
        //      - pour tout noeud : e!=null implique t != null, et
        //      - pour tout noeud tq t != null, alors aucune case de t ne contient null
        // - tous les elements ont p pour préfixe de clef, et
        // - si on ignore le préfixe p de toutes les clefs alors this est un Ptree

        if (e==null && t==null)
            return true;
        if (e != null) {
            if (t == null)
                return false;
            if (!e.getKey().equals(p)) {//si il y a un élément alors sa clef complète doit être p
                return false;
            }
        }
        //donc t!=null
        boolean ok = true;
        int i = 0;
        while (ok && i < t.size()) {
            Ptree<T> fils = t.get(i);
            if (fils == null)
                ok = false;
            else {
                p.add(i);
                ok = fils.isPtree(p);
                p.remove(p.size() - 1);
            }
            i++;
        }
        return ok;

    }
    public boolean contains(T el){
        return contains(el,0);
    }
    public boolean contains(T el, int i){
        //retourne vrai ssi contient élément e tq e.getKey()[i..] = el.getKey()[i..]
        if(isEmpty())
            return false;
        if(el.getKeyLength()==i)
            return (e!=null);
        return t.get(el.getDigit(i)).contains(el, i + 1);
    }
    public boolean delete(T el){
        return delete(el,0);
    }
    public boolean estFeuille(){
        if(isEmpty())
            return false;
        boolean res = true;
        int i=0;
        while(i<t.size() && res){
            res=t.get(i).isEmpty();
            i++;
        }
        return res;
    }
    public boolean delete(T el, int i) {
        //soit c = el.getKey()[i..]
        //si il existe un élément e tq e.getKey()[i..] = el.getKey()[i..] alors efface cet élément et retourne vrai,
        //sinon ne fait rien et retourne faux (attention, lorsque l'on efface un élément il faut maintenir la strucutre d'arbre préfixe,
        //par exemple si l'on contenait un unique élément de clef [1,2,3] (et donc que nbSommet() valait 4), alors la suppression
        //de cet élément doit redonner un Ptree vide
        if (i == el.getKeyLength()) {
            boolean res = e!=null;
            e = null;
            if(estFeuille())
                t=null;
            return res;
        } else {
            if (!isEmpty()) {
                int d = el.getDigit(i);
                boolean res = t.get(d).delete(el, i + 1);

                //si le sous arbre d est devenu vide et que e==null, alors il y a une vérif à faire :
                //si toutes les cases de d sont vides maintenant,
                //this est devenu une feuille, et comme e==null il faut transformer this en arbre vide
                //(va permettre de supprimer longues chaines dans arbre à la suite de suppression d'un élém)

                if (t.get(d).isEmpty() && e==null) {

                    boolean trouveNonVide = false;
                    int j = 0;
                    while (j < t.size() && !trouveNonVide) {
                        if (!t.get(j).isEmpty()) {
                            trouveNonVide = true;
                        }
                        j++;
                    }
                    if (!trouveNonVide) {
                        t = null;
                    }
                }
                return res;
            }
            else{
                return false;
            }
        }
    }



    ////////////////////////////
    public ArrayList<T> listAll(){
        return listAllV0(new ArrayList<Integer>());
    }
    public ArrayList<T> listAllV0(ArrayList<Integer> p){
        /* retourne la liste de tous les éléments du sous arbre enraciné en v_p = sommet obtenu en suivant la clef p (et si v_p n'existe pas alors retourne vide),
         * par ordre alphabetique*/

        ArrayList<T> res = new ArrayList<>();
        if(e!=null && p.isEmpty())
            res.add(e);
        if(!isEmpty()){
            if(p.isEmpty()) {
                for (Ptree<T> fils : t) {
                    res.addAll(fils.listAllV0(p));
                }
            }
            else {
                int d = p.remove(0);
                res.addAll(t.get(d).listAllV0(p));

            }
        }
        return res;
    }

    public ArrayList<T> listAllBetween(ArrayList<Integer> p1, ArrayList<Integer>p2) {
        return listAllBetween(p1,p2,false);
    }
    public ArrayList<T> listAllBetween(ArrayList<Integer> p1, ArrayList<Integer>p2, boolean p2infinite) {
        //soit p2' = p2 si p2infinite faux, et +infini sinon
        //retourne liste de tous les éléments et tq p1 <= e.getKey() <= p2'
        ArrayList<T> res = new ArrayList<>();
        if(e!=null && p1.isEmpty())
            res.add(e);
        if(!isEmpty()) {
            ArrayList<Integer> p1pref = new ArrayList<>();
            ArrayList<Integer> p2pref = new ArrayList<>();

            int d1 = 0;
            int d2 = -1;
            if (!p1.isEmpty()) {
                d1 = p1.get(0);
                p1.remove(0);
                p1pref = p1;
            }
            if (!p2.isEmpty()) {
                d2 = p2.get(0);
                p2.remove(0);
                p2pref = p2;
            }
            if(p2infinite)
                d2 = t.size()-1;

            for (int i = d1; i <= d2; i++) {
                if ((i == d1) && (i == d2)) {

                    res.addAll(t.get(i).listAllBetween(p1pref, p2pref,p2infinite));
                } else if (i == d1) {
                    res.addAll(t.get(i).listAllBetween(p1pref, new ArrayList<Integer>(),true));
                } else if (i == d2) {
                    res.addAll(t.get(i).listAllBetween(new ArrayList<Integer>(), p2pref,p2infinite));
                } else {
                    res.addAll(t.get(i).listAllBetween(new ArrayList<Integer>(), new ArrayList<>(),true));
                }
            }
        }

        return res;
    }


    /************************************************************************
     section optimisations
     ************************************************************************/
    private  ArrayList<Ptree<T>> buildTabPtreeVides(int taille){
        ArrayList<Ptree<T>> res = new ArrayList<>();
        for(int i=0;i<taille;i++){
            res.add(new Ptree<T>());
        }
        return res;
    }
    public void remplirFilsNonVides(){
        if(!isEmpty()){
            for(int i=0;i<t.size();i++){
                if(!t.get(i).isEmpty())
                    listeFilsNonVides.add(i);
                t.get(i).remplirFilsNonVides();
            }
        }
    }
    public Ptree<T> remplirRaccourcis(){
        //action
        // 1)modifie this pour que pour tout sous arbre a de this (y compris a=this),
        //a.t.get(i) est vide, alors a.raccourcis.get(i) est vide
        //sinon, a.raccourcis.get(i) pointe vers le sommet utile le moins profond de a.t.get(i)
        // 2) retourne le somme utile le moins profond si il en existe un, et l'arbre vide sinon

        if(isEmpty())
            return this;
        int cpt = 0;
        int lastNonEmpty=-1;
        for(int i=0;i<t.size();i++){
            if(!t.get(i).isEmpty()) {
                cpt++;
                lastNonEmpty = i;
            }
            Ptree<T> res = t.get(i).remplirRaccourcis();
            raccourcis.add(res);
        }
        if(e!=null || cpt>=2)//on a un élément, ou on est un noeud de branch
            return this;
        //cpt vaut forcement 1, donc seul fils non vide dans lastNonEmpty
        return raccourcis.get(lastNonEmpty);
    }
    public ArrayList<T> listAllV1(ArrayList<Integer> p){
        /*
        //prérequis : rempliFilsNonVides() a été appelé
        retourne la liste de tous les éléments du sous arbre enraciné en v_p = sommet obtenu en suivant p (et si v_p n'existe pas alors retourne vide),
         * par ordre alphabetique, en ne visitant que les sommets non vides (sauf si this est réduit à l'arbre vide)*/


        ArrayList<T> res = new ArrayList<>();
        if(e!=null && p.isEmpty())
            res.add(e);
        if(!isEmpty()){
            if(p.isEmpty()) {

                for(Integer i : listeFilsNonVides){
                    res.addAll(t.get(i).listAllV1(p));
                }
            }
            else {
                int d = p.remove(0);
                res.addAll(t.get(d).listAllV1(p));

            }
        }
        return res;
    }


    public int nbSommetsVisitesV0(ArrayList<Integer> p){
        /* retourne le nombre de sommets visités par listAllV0(p) */

        if(!isEmpty()){
            if(p.isEmpty()) {
                return nbSommetsTotal();
            }
            else {
                int d = p.remove(0);
                return 1+t.get(d).nbSommetsVisitesV0(p);

            }
        }
        return 1;
    }
    public int nbSommetsVisitesV1(ArrayList<Integer> p){
        /* prérequis : remplirFilsNonVide() a été appelé
        action : retourne le nombre de sommets visités par listAllV0(p) */

        if(!isEmpty()){
            if(p.isEmpty()) {
                return nbSommets();
            }
            else {
                int d = p.remove(0);
                return 1+t.get(d).nbSommetsVisitesV1(p);

            }
        }
        return 1;
    }
    public ArrayList<T> listAllV2(ArrayList<Integer> p){
        /*prérequis : rempliFilsNonVides() et remplirRaccourcis ont été appelés
        retourne la liste de tous les éléments du sous arbre enraciné en v_p = sommet obtenu en suivant p (et si v_p n'existe pas alors retourne vide),
                 par ordre alphabetique, en ne visitant que les sommets utiles (sauf éventuellement la racine que l'on visite de toute façon)*/
        ArrayList<T> res = new ArrayList<>();
        if(e!=null && p.isEmpty())
            res.add(e);
        if(!isEmpty()){
            if(p.isEmpty()) {
                for(Integer i : listeFilsNonVides){
                    res.addAll(raccourcis.get(i).listAllV2(p));
                }
            }
            else {
                int d = p.remove(0);
                res.addAll(t.get(d).listAllV2(p));

            }
        }
        return res;
    }

    public int nbSommetsUtiles(){

        //prérequis : rempliFilsNonVides et remplirRaccourcis ont été appelés
        if(isEmpty())
            return 0;
        int res=0;
        if((e!=null)||(listeFilsNonVides.size()>=2))
            res++;
        for(Integer i : listeFilsNonVides){
            res+=raccourcis.get(i).nbSommetsUtiles();
        }
        return res;
    }
    public int nbSommetsVisitesV2(ArrayList<Integer> p){
        //prérequis : rempliFilsNonVides et remplirRaccourcis ont été appelés
        /* retourne une borne supérieure (à 1 près) sur le nombre de sommets visités par listAllV2(p) */

        int res=1;
        if(!isEmpty()){
            if(p.isEmpty()) {
                return res+nbSommetsUtiles();
            }
            else {
                int d = p.remove(0);
                res+=t.get(d).nbSommetsVisitesV2(p);

            }
        }
        return res;
    }
    /*
    public ArrayList<T> listAllNonNulElemIte(){

        ArrayList<T> res = new ArrayList<>();
        if(!isEmpty()){
            ArrayList<Ptree<T>> todo = new ArrayList<>();
            todo.add(this);//ensemble à regarder, tous non vides dedans
            while(!todo.isEmpty()) {
                Ptree<T> current = todo.remove(0);
                if (current.e != null)
                    res.add(e);
                for (Integer i : current.listeFilsNonVides) {
                    todo.add(current.raccourcis.get(i));
                }
            }
        }
        return res;
    }
    */

    /*
    public ArrayList<T> listAllV2Ite(ArrayList<Integer> p){
        //retourne la liste de tous les éléments du sous arbre enraciné en v_p = sommet obtenu en suivant p (et si v_p n'existe pas alors retourne vide),
        // par ordre alphabetique

        //a appeler après avoir appelé rempliFilsNonVides et remplirRaccourcis
        ArrayList<T> res = new ArrayList<>();
        if(e!=null && p.isEmpty())
            res.add(e);
        if(!isEmpty()){
            if(p.isEmpty()) {
                res.addAll(listAllNonNulElemIte());
            }
            else {
                int d = p.remove(0);
                res.addAll(t.get(d).listAllV2(p));

            }
        }
        return res;
    }
    */

    /************************************************************************
     section affichage text et svg
     ************************************************************************/
    public String toStringAux(String s){
        if(isEmpty()) return s+"\n";
        else{
            String res = s;
            if(e!=null)
                res+=e+"\n";
            else
                res+="\n";

            for(int l=0;l<t.size();l++){
                // if(!t.get(l).isEmpty()) {
                res += s + "fils " + l + " :\n";
                res += t.get(l).toStringAux(s + "   ");
                //}
            }

            return res;
        }
    }

    @Override
    public String toString() {
        return toStringAux("");
    }

    public SVG toSVG(boolean printEmptyTries){
        if(printEmptyTries)
            return toSVG1();
        else
            return toSVG2();
    }
    public SVG toSVG2(){
        double w = getWidth2(0);//(getMaxDigit()*40)*10;
        double hlayer = Math.max(w/1000,20*hboxTab);
        double h=hlayer*(getHeight()+1);
        SVG res = new SVG(w,h);
        Group g = this.toGroup2(0,0,w,hlayer,"");
        Rectangle r = new Rectangle(0,0,w,h);

        Style s = new Style();
        s.setFill("white");
        s.setFillOpacity(1.0);
        r.setStyle(s);
        res.add(r);
        res.add(g);
        return res;
    }
    public double getWidth2(int i){//0 <= i : profondeur courante (et donc longuer du préfixe courant qu'on affiche à chaque noeud)
        if(isEmpty())
            return 0;
        // if(estFeuille()) {


        int t1=0;
        if(e!=null)
            t1 = e.toString().length() * fontsizeElem; //taille pour ecrire e

        int t2 = (i+3) * fontsizeElem; //taille pour écrire le préfixe sous la forme "p: xxxx" avec xxx le préfixe courant
        int t3 = (t.size()+2)*(int)hboxTab;
        //  return Collections.max(Arrays.asList(t1,t2,textEmpty.length()*fontsizeElem,textEnull.length()*fontsizeElem));
        //}
        int res = 0;
        for(Ptree<T> fils : t){
            if(!fils.isEmpty())
                res+=fils.getWidth2(i+1);
        }
        return Collections.max(Arrays.asList(t1,t2,textEmpty.length()*fontsizeElem,textEnull.length()*fontsizeElem,t3,res));
        //  return res;
    }
    public Group toGroup2(double cx, double cy, double w, double h, String p) {
        //largeur w, hauteur h entre couches
        //p préfixe à ce niveau
        Group g = new Group();

        //double hbox = 2*hboxTab; //h * 3 / 8; //hauteur pour dessiner tableau et elem
        //double hedges = h - hbox; //hauteur pour dessiner aretes vers fils

        //on va dessiner toStringelement avec bonne fontsize, puis hbox/3 vide, puis tab hauteur hbox/3



        if (isEmpty()) {

            return g;
        } else {
            // Text tpref = new Text("p: "+p, cx+w/2-(p.length()+3)/2, hboxTab+cy+fontsizeElem, fontsizeElem);
            Text tpref = new Text("p: "+p, cx+w/2 - (t.size() / 2) * hboxTab, hboxTab+cy+fontsizeElem, fontsizeElem);
            g.add(tpref);
            Group ge = new Group();
            if (e == null) {
                // ge.add(new Text(textEnull, cx+w/2 -textEnull.length()/2, cy+fontsizeElem, fontsizeElem));
                ge.add(new Text(textEnull, cx+w/2  - (t.size() / 2) * hboxTab, cy+fontsizeElem, fontsizeElem));
            }
            else{
                // ge.add(new Text(e.toString(), cx+w/2-e.toString().length()/2, cy+fontsizeElem, fontsizeElem));
                ge.add(new Text(e.toString(), cx+w/2 - (t.size() / 2) * hboxTab, cy+fontsizeElem, fontsizeElem));
            }

            Group gt = new Group();
            //if (t == null)
            //   gt.add(new Text(textTnull, cx+w/2 - 5, cy + 3*hboxTab, fontsizeElem));
            //else {
            //if(!estFeuille()) {
            double startx = cx + w / 2 - (t.size() / 2) * hboxTab;
            for (int i = 0; i < t.size(); i++) {
                Rectangle r = new Rectangle(startx + i * hboxTab, cy + 3 * hboxTab, hboxTab, hboxTab);
                Style sr = new Style();
                sr.setFill("none");
                sr.setStroke("black");
                r.setStyle(sr);
                gt.add(r);
            }
            double wrec = w / t.size();
            double current = cx;
            for (int i = 0; i < t.size(); i++) {
                if(!t.get(i).isEmpty()) {
                    Line l = new Line(startx + hboxTab / 2 + i * hboxTab, cy + 3 * hboxTab + hboxTab / 2, current + t.get(i).getWidth2(p.length() + 1) / 2, cy + h);//cx + wrec/2+i*wrec, cy + h);
                    Style styleL = new Style();
                    styleL.setStroke("black");
                    l.setStyle(styleL);
                    gt.add(l);
                    //g.add(t.get(i).toGroup(cx+i*wrec,cy+h,wrec,h));
                    g.add(t.get(i).toGroup2(current, cy + h, t.get(i).getWidth2(p.length() + 1), h, p + " " + i));
                    current += t.get(i).getWidth2(p.length() + 1);
                }
            }
            //  }
            //  }
            g.add(ge);
            g.add(gt);
            return g;
        }
    }
    public SVG toSVG1(){
        double w = getWidth1(0);//(getMaxDigit()*40)*10;
        double hlayer = Math.max(w/1000,20*hboxTab);
        double h=hlayer*(getHeight()+1);
        SVG res = new SVG(w,h);
        Group g = this.toGroup1(0,0,w,hlayer,"");
        Rectangle r = new Rectangle(0,0,w,h);

        Style s = new Style();
        s.setFill("white");
        s.setFillOpacity(1.0);
        r.setStyle(s);
        res.add(r);
        res.add(g);
        return res;
    }
    public double getWidth1(int i){//0 <= i : profondeur courante (et donc longuer du préfixe courant qu'on affiche à chaque noeud)
        if(isEmpty())
            return fontsizeElem*2;
        if(estFeuille()) {



            int t1 = e.toString().length() * fontsizeElem; //taille pour ecrire e
            int t2 = (i+3) * fontsizeElem; //taille pour écrire le préfixe sous la forme "p: xxxx" avec xxx le préfixe courant

            int tsousArbres = 0;//largeur pour afficher les maxDigits sous arbres vides
            for(Ptree<T> fils : t)
                tsousArbres+=fils.getWidth1(i+1);


            //si on est une feuille il faut avoir la largeur pour écrire tous les termes du max,
            //le dernier étant la largeur pour afficher

            return Collections.max(Arrays.asList(t1,t2,textEmpty.length()*fontsizeElem,textEnull.length()*fontsizeElem,tsousArbres));
        }
        int res = 0;
        for(Ptree<T> fils : t){
            res+=fils.getWidth1(i+1);
        }
        return res;
    }
    public Group toGroup1(double cx, double cy, double w, double h, String p) {
        //largeur w, hauteur h entre couches
        //p préfixe à ce niveau
        Group g = new Group();

        //double hbox = 2*hboxTab; //h * 3 / 8; //hauteur pour dessiner tableau et elem
        //double hedges = h - hbox; //hauteur pour dessiner aretes vers fils

        //on va dessiner toStringelement avec bonne fontsize, puis hbox/3 vide, puis tab hauteur hbox/3



        if (isEmpty()) {
            Text t = new Text(textEmpty, cx+w/2, cy+fontsizeElem, fontsizeElem);
            g.add(t);

            return g;
        } else {
            // Text tpref = new Text("p: "+p, cx+w/2-(p.length()+3)/2, hboxTab+cy+fontsizeElem, fontsizeElem);
            Text tpref = new Text("p: "+p, cx+w/2 - (t.size() / 2) * hboxTab, hboxTab+cy+fontsizeElem, fontsizeElem);
            g.add(tpref);
            Group ge = new Group();
            if (e == null) {
                // ge.add(new Text(textEnull, cx+w/2 -textEnull.length()/2, cy+fontsizeElem, fontsizeElem));
                ge.add(new Text(textEnull, cx+w/2  - (t.size() / 2) * hboxTab, cy+fontsizeElem, fontsizeElem));
            }
            else{
                // ge.add(new Text(e.toString(), cx+w/2-e.toString().length()/2, cy+fontsizeElem, fontsizeElem));
                ge.add(new Text(e.toString(), cx+w/2 - (t.size() / 2) * hboxTab, cy+fontsizeElem, fontsizeElem));
            }
            Group gt = new Group();
            //if (t == null)
            //   gt.add(new Text(textTnull, cx+w/2 - 5, cy + 3*hboxTab, fontsizeElem));
            //else {
            double startx = cx+w/2 - (t.size() / 2) * hboxTab;
            for (int i = 0; i < t.size(); i++) {
                Rectangle r = new Rectangle(startx + i * hboxTab, cy + 3*hboxTab, hboxTab, hboxTab);
                Style sr = new Style();
                sr.setFill("none");
                sr.setStroke("black");
                r.setStyle(sr);
                gt.add(r);
            }
            double wrec = w/t.size();
            double current = cx;
            for(int i=0;i<t.size();i++){
                Line l = new Line(startx+hboxTab/2+i*hboxTab, cy + 3*hboxTab+hboxTab/2, current+t.get(i).getWidth1(p.length()+1)/2,cy+h);//cx + wrec/2+i*wrec, cy + h);
                Style styleL = new Style();
                styleL.setStroke("black");
                l.setStyle(styleL);
                gt.add(l);
                //g.add(t.get(i).toGroup(cx+i*wrec,cy+h,wrec,h));
                g.add(t.get(i).toGroup1(current, cy + h, t.get(i).getWidth1(p.length() + 1), h, p + " " + i));
                current+=t.get(i).getWidth1(p.length()+1);
            }
            //  }
            g.add(ge);
            g.add(gt);
            return g;
        }
    }





}
