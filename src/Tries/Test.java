
package Tries;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;


public class Test {
    public static void printTest(String msg, boolean b){
        System.out.println("test : "+(b?" ok":" KO") + " (" + msg + ")");
    }


    public static void testAdd1() {
        Person a1 = new Person(11);
        Person a2 = new Person(112);
        Person a3 = new Person(113);
        Person a4 = new Person(23);


        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        ta.add(a2);
        ta.add(a3);
        ta.add(a4);

        printTest("testAdd1", ta.isPtree());
    }
    public static void testNbSommets() {
        Person a1 = new Person(11);
        Person a2 = new Person(112);
        Person a3 = new Person(113);
        Person a4 = new Person(23);


        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        ta.add(a2);
        ta.add(a3);
        ta.add(a4);

        printTest("nbSommets", ta.nbSommets()==7);
        printTest("nbSommetsTot", ta.nbSommetsTotal()==7+9*7+1);

    }
    public static void testGetHeight() {
        Person a1 = new Person(1);
        Person a2 = new Person(12);
        Person a3 = new Person(1234);
        Person a4 = new Person(1235);



        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        printTest("getHeight", ta.getHeight()==2);
        ta.add(a2);
        ta.add(a3);
        ta.add(a4);

        printTest("getHeight", ta.getHeight()==5);

    }

    public static void testSize() {
        Person a1 = new Person(11);
        Person a2 = new Person(112);
        Person a3 = new Person(113);
        Person a4 = new Person(23);



        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        ta.add(a2);
        ta.add(a3);
        ta.add(a4);

        printTest("size", ta.size()==4);
    }

    public static void testContains() {
        Person b1 = new Person(10);
        Person b2 = new Person(11232);
        Person b3 = new Person(112);
        Person b4 = new Person(11);
        Person b5 = new Person(104);
        Person b6 = new Person(112323);

        Ptree<Person> tb = new Ptree<>();
        tb.add(b1);
        tb.add(b2);
        tb.add(b3);

        printTest("contains b1", tb.contains(b1));
        printTest("contains b2", tb.contains(b2));
        printTest("contains b3", tb.contains(b3));
        printTest("does not contains b4", !tb.contains(b4));
        printTest("does not contains b5", !tb.contains(b5));
        printTest("does not contains b6", !tb.contains(b6));

    }


    public static void testAdd2() {

        Ptree<Person> t = new Ptree<>();
        Random r = new Random();
        ArrayList<Person> liste = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            Person a = new Person(r.nextInt(500));
            t.add(a);
            if (!liste.contains(a))
                liste.add(a);
        }
        printTest("testAdd2", t.size() == liste.size());
    }

    public static void testEFficaciteMemoire() {

        Ptree<Person> t = new Ptree<>();
        Random r = new Random();
        ArrayList<Person> liste = new ArrayList<>();
        for (int i = 0; i < 20000; i++) {
            Person a = new Person(100000000+r.nextInt(899999999));
            t.add(a);
            if (!liste.contains(a))
                liste.add(a);
        }

        System.out.println("nb sommets pour " + t.size() + " éléments ayant chacun une clef de 9 chiffres " + t.nbSommets() + " <= " + t.size() +" x 9");
    }

    public static void testDelete() {
        Person b1 = new Person(10);
        Person b2 = new Person(11232);
        Person b3 = new Person(112);
        Ptree<Person> tb = new Ptree<>();
        tb.add(b1);
        tb.add(b2);
        tb.add(b3);
        tb.delete(b2);

        printTest("delete b2", !tb.contains(b2));
        printTest("delete b2", tb.isPtree());
        printTest("delete b2", tb.contains(b1) && tb.contains(b3));

        tb.add(b2);

        tb.delete(b3);
        printTest("delete b3", !tb.contains(b3));
        printTest("delete b3", tb.isPtree());
        printTest("delete b3", tb.contains(b1) && tb.contains(b2));


    }

    //à supprimer pour étudiants car méthode fournie
    public static void testIsPtree(){
        Person a1 = new Person(102);
        Person a2 = new Person(201);
        Person a3 = new Person(1123);
        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        printTest("IsPtree2",ta.isPtree());
        ta.add(a2);
        printTest("IsPtree3",ta.isPtree());
        ta.add(a3);
        printTest("IsPtree4",ta.isPtree());

        ta.getT().get(1).getT().get(1).setE(new Person(1123));
        //ta a maintenant un élément qui ne respecte pas les préfixe
        printTest("IsPtree5",!ta.isPtree());

        ta.getT().get(1).getT().get(1).setE(new Person(11));
        //doit etre ok à nouveau
        printTest("IsPtree6",ta.isPtree());

        ArrayList<Ptree<Person>> backup = ta.getT().get(1).getT().get(0).getT().get(2).getT();
        ta.getT().get(1).getT().get(0).getT().get(2).setT(null);
        printTest("IsPtree7",!ta.isPtree());
        ta.getT().get(1).getT().get(0).getT().get(2).setT(backup);



        ta.getT().get(1).getT().set(1,null);
        //ta a maintenant un sous arbre avec un tableau mal défini (une case à null)
        //ta.toSVG(true).saveAsFile("taFaux2.svg");
        printTest("IsPtree8",!ta.isPtree());

    }









    public static void testListAllV0() {
        Person b1 = new Person(11);
        Person b2 = new Person(112);
        Person b3 = new Person(1121);
        Person b4 = new Person(11232);
        Person b5 = new Person(11235);
        Person b6 = new Person(1124);
        Person b7 = new Person(24);

        Ptree<Person> tb = new Ptree<>();
        tb.add(b1);
        tb.add(b2);
        tb.add(b3);
        tb.add(b4);
        tb.add(b5);
        tb.add(b6);
        tb.add(b7);

        ArrayList<Integer> p1 = new ArrayList<>(Arrays.asList(1,1,2));
        ArrayList<Person> l1 = new ArrayList<>(Arrays.asList(b2,b3,b4,b5,b6));
        printTest("listallV0", tb.listAllV0(p1).equals(l1));

        ArrayList<Integer> p2 = new ArrayList<>(Arrays.asList(1,1,2,3));
        ArrayList<Person> l2 = new ArrayList<>(Arrays.asList(b4,b5));
        printTest("listallV0", tb.listAllV0(p2).equals(l2));

    }

    public static void testListAllV1() {
        Person b1 = new Person(11);
        Person b2 = new Person(112);
        Person b3 = new Person(1121);
        Person b4 = new Person(11232);
        Person b5 = new Person(11235);
        Person b6 = new Person(1124);
        Person b7 = new Person(24);

        Ptree<Person> tb = new Ptree<>();
        tb.add(b1);
        tb.add(b2);
        tb.add(b3);
        tb.add(b4);
        tb.add(b5);
        tb.add(b6);
        tb.add(b7);

        tb.remplirFilsNonVides();
        ArrayList<Integer> p1 = new ArrayList<>(Arrays.asList(1,1,2));
        ArrayList<Person> l1 = new ArrayList<>(Arrays.asList(b2,b3,b4,b5,b6));
        printTest("listallV1", tb.listAllV1(p1).equals(l1));

        ArrayList<Integer> p2 = new ArrayList<>(Arrays.asList(1,1,2,3));
        ArrayList<Person> l2 = new ArrayList<>(Arrays.asList(b4,b5));
        printTest("listallV1", tb.listAllV1(p2).equals(l2));

    }

    //à supprimer pour eux
    public static void testListAllBetween() {
        Person b1 = new Person(11);
        Person b2 = new Person(112);
        Person b3 = new Person(1121);
        Person b4 = new Person(11232);
        Person b5 = new Person(11235);
        Person b6 = new Person(1124);
        Person b7 = new Person(24);

        Ptree<Person> tb = new Ptree<>();
        tb.add(b1);
        tb.add(b2);
        tb.add(b3);
        tb.add(b4);
        tb.add(b5);
        tb.add(b6);
        tb.add(b7);
        ArrayList<Integer> p1 = new ArrayList<>(Arrays.asList(1,1,2));
        ArrayList<Integer> p2 = new ArrayList<>(Arrays.asList(1,2));//,2,3,6,6));
        ArrayList<Person> l3 = new ArrayList<>(Arrays.asList(b2,b3,b4,b5,b6));
        printTest("list all between 112 et 12", tb.listAllBetween(p1,p2).equals(l3));


        ArrayList<Integer> p3inf = new ArrayList<>(Arrays.asList(1,1,2,0));
        ArrayList<Integer> p3sup = new ArrayList<>(Arrays.asList(1,1,2,3,4));
        ArrayList<Person> l4 = new ArrayList<>(Arrays.asList(b3,b4));
        printTest("list all between 1120 et 11234", tb.listAllBetween(p3inf,p3sup).equals(l4));
    }



    public static void testListAllV2() {
        Person a1 = new Person(13);
        Person a2 = new Person(112);
        Person a3 = new Person(113);

        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        ta.add(a2);
        ta.add(a3);

        ta.remplirRaccourcis();
        ta.remplirFilsNonVides();
        ArrayList<Integer> p1 = new ArrayList<>(Arrays.asList(1));
        printTest("listallV2 prefix 1", ta.listAllV2(p1).size()==3);
        p1 = new ArrayList<>(Arrays.asList(1,1));
        printTest("listallV2 prefix 2", ta.listAllV2(p1).size()==2);

    }

    /*
    public static void testremplirRac() {

        Person a1 = new Person(1234567);
        Person a2 = new Person(1234568);
        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        ta.add(a2);
        ta.remplirPrefD(new ArrayList<>());
        ta.remplirRaccourcis();
        ta.remplirFilsNonVides();
        System.out.println(ta.listAllV2int(new ArrayList<>()));
        ta.listAllV2(new ArrayList<>());
    }
    */

    /* //test pour moi car méthodes fournies
    public static void testNbSommetsVisites() {

        Person a1 = new Person(1111);
        Person a2 = new Person(1122);
        Person a3 = new Person(21);
        Person a4 = new Person(221);
        Person a5 = new Person(222);
        Person a6 = new Person(223);
        Person a7 = new Person(111);

        Person a8 = new Person(12);
        Person a9 = new Person(2);

        Ptree<Person> ta = new Ptree<>();

        ta.add(a7);
        ta.add(a8);
        ta.add(a9);
        ArrayList<Integer> p1 = new ArrayList<>();//Arrays.asList(1));
        ta.remplirRaccourcis();
        ta.remplirFilsNonVides();

        System.out.println(ta.nbSommetsVisitesV0(p1));
        ta.listAllV0(p1);
        System.out.println(ta.nbSommetsVisitesV1(p1));
        ta.listAllV1(p1);




    }
    */

    /* test inutile pour eux car méthode fournie
    public static void testNbUtiles() {
        Person b1 = new Person(10);
        Person b2 = new Person(11232);
        Person b3 = new Person(112);
        Ptree<Person> tb = new Ptree<>();
        tb.add(b1);
        tb.add(b2);
        tb.add(b3);

        tb.remplirRaccourcis();
        tb.remplirFilsNonVides();

        printTest("nbUtiles", tb.nbSommetsUtiles() == 4);
    }
*/
    public static void main(String[] args){
        testIsPtree();
        testAdd1();
        testAdd2();
        testSize();
        testDelete();
        testContains();
        testEFficaciteMemoire();
        testListAllV0();
        testListAllV1();
        testNbSommets();
        testGetHeight();
        testListAllV2();




    }

}
